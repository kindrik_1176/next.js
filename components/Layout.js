const { default: Navbar } = require("./Navbar")
const { default: Footer } = require("./Footer")

const Layout = ({ children }) => {
    return (
        <div>
            <Navbar />
            {children}
            <Footer />
        </div>
    );
}

export default Layout;