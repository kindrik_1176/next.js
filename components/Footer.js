const Footer = () => {
    return (
        <footer>
            <p>Copyright 2021 Josh Winters</p>
        </footer>
    );
}

export default Footer;