import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Link from 'next/link'

export default function Home() {
  return (
    <div className={styles.title}>
      <h1>Home Page Content.</h1>
      <p className={styles.text}>Placeholder Text</p>
      <Link href='/clients'>
        <a className={styles.btn}>See client listing</a>
      </Link>
    </div>
  )
}
