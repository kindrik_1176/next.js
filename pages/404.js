
import Link from 'next/link'
import { useEffect } from 'react'
import { useRouter } from 'next/router'


const NotFound = () => {
    const router = useRouter();

    useEffect(() => {
        setTimeout(() => {
            router.push('/');
        }, 3000)
    }, [])

    return(
        <div className='not-found'>
            <h1>Error 404</h1>
            <p>An error has ocurred, that page cannot be found.</p>
            <p>Back to <Link href='/'><a>Homepage</a></Link></p>
        </div>
    )
}

export default NotFound;